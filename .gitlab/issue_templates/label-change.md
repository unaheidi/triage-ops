## Summary

Please list the Stage or Group labels changes required. The change can fall under the 3 scenarios below:
* Splitting a Stage into multiple Groups
* Changing the Categories in a Stage
* Adding a new Stage

For every case above, the Engineering Productivity needs to ensure that:
* The label change is factored into the triage mechanism.
* Old labels are migrated correctly on affected Issues and Merge Requests.
* Engineering Dashboards in https://quality-dashboard.gitlap.com/groups/gitlab-org is setup correctly as well.

## Action items

* [ ] Label migration on existing Issues and Merge Requests: apply the new label to closed/merged and in-progress work. 
* [ ] Dashboard creation: create new dashboard view for new stage/group categories https://quality-dashboard.gitlap.com/groups/gitlab-org, location `gitlab_insights.rb`
* [ ] If applicable, archive the old labels with renaming and adding "DEPRECATED" at the end of the label name.
* [ ] If applicable, delete the old dashboard views using the deprecated labels.

/cc @godfat @markglenfletcher @rymai @meks
