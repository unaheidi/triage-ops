# frozen_string_literal: true

require_relative 'devops_labels'

module MrLabelMigrationContext
  include DevopsLabels::Context

  def stage_or_group_label_from_legacy_label
    if single_legacy_label?
      group = group_from_team_label if !has_group_label? && DevopsLabels::TEAM_TO_GROUP[current_team_label]

      inferred_stage = group ? stage_for_group(group) : stage_from_team_label
      stage = inferred_stage unless has_stage_label?

      LabelsInferenceResult.new([label_for(stage: stage), label_for(group: group)], current_team_label)
    else
      LabelsInferenceResult.new
    end
  end

  def comment_for_merge_request
    stage_or_group_label_from_legacy_label.comment if stage_or_group_label_from_legacy_label.any?
  end

  def single_legacy_label?
    (DevopsLabels.team_labels & label_names).length == 1
  end

  def can_infer_from_legacy_label?
    single_legacy_label?
  end
end
