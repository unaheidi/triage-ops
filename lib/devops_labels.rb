# frozen_string_literal: true

class DevopsLabels
  SECTIONS = %w[
    dev cicd ops secure defend growth enablement
  ].freeze
  STAGES_PER_SECTION = {
    'dev' => %w[manage plan create],
    'cicd' => %w[verify package release],
    'ops' => %w[configure monitor],
    'secure' => %w[secure],
    'defend' => %w[defend],
    'growth' => %w[growth],
    'enablement' => %w[enablement]
  }.freeze
  GROUPS_PER_STAGE = {
    'manage' => %w[access measure],
    'plan' => ['team planning', 'portfolio management', 'certify'],
    'create' => ['source code', 'knowledge', 'editor', 'gitaly', 'gitter'],
    'verify' => ['ci and runner', 'testing'],
    'package' => %w[package],
    'release' => ['progressive delivery', 'release management'],
    'configure' => ['autodevops and kubernetes', 'serverless and paas'],
    'monitor' => %w[apm health],
    'secure' => ['static analysis', 'dynamic analysis', 'software composition analysis'],
    'defend' => ['runtime application security', 'threat management', 'application infrastructure security'],
    'growth' => %w[activation adoption upsell retention fulfillment],
    'enablement' => %w[distribution geo memory ecosystem]
  }.freeze
  CATEGORIES_PER_GROUP = {
    'access' => [
      '2FA',
      'admin dashboard',
      'audit events',
      'authentication',
      'authorization',
      'group',
      'group templates',
      'groups',
      'ldap',
      'oauth',
      'permissions',
      'saml',
      'spam fighting',
      'subgroups',
      'user management',
      'user profile'
    ],
    'measure' => [
      'analytics',
      'convdev',
      'cycle analytics',
      'gitlab.com',
      'import',
      'importers',
      'internationalization',
      'navigation',
      'project',
      'project export',
      'project import',
      'project templates'
    ],
    'team planning' => [
      'Category::Issue Boards',
      'Category::Project Management',
      'Category::Time Tracking',
      'filter bar',
      'issue weight',
      'issues',
      'Jira',
      'labels',
      'markdown',
      'milestones',
      'notifications',
      'quick actions',
      'time tracking',
      'todos'
    ],
    'portfolio management' => [
      'Category::Agile Portfolio Management',
      'Category::Value Stream Management',
      'epics',
      'roadmaps'
    ],
    'certify' => [
      'Category::Quality Management',
      'Category::Requirements Management',
      'Category::Service Desk'
    ],
    'source code' => [
      'approvals',
      'code review',
      'diff',
      'encoding',
      'forking',
      'image diff',
      'lfs',
      'merge requests',
      'mirror',
      'repository'
    ],
    'knowledge' => [
      'design management',
      'wiki'
    ],
    'editor' => [
      'elasticsearch',
      'live coding',
      'search',
      'snippets',
      'web edit',
      'web ide'
    ],
    'gitaly' => [
      'Gitaly HA',
      'Gitaly n+1'
    ],
    'gitter' => [],
    'ci and runner' => [
      'Category::Continuous Integration',
      'pipeline',
      'Category::Runner'
    ],
    'testing'=> [
      'Category::Accessibility Testing',
      'Category::Code Quality',
      'Category::Compatibility Testing',
      'Category::Continuous Integration',
      'Category::Performance Testing',
      'Category::System Testing',
      'Category::Usability testing'
    ],
    'package'=> [
      'Container Registry',
      'Dependency Proxy',
      'Helm Chart Registry',
      'Package Registry',
    ],
    'progressive delivery' => [
      'continuous delivery',
      'incremental rollout',
      'release governance',
      'release orchestration'
    ],
    'release management' => [
      'feature flags',
      'pages',
      'review apps',
      'secrets management'
    ],
    'autodevops and kubernetes' => [
      'auto devops',
      'chaos engineering',
      'chatops',
      'runbooks'
    ],
    'serverless and paas' => [
      'PaaS',
      'Serverless'
    ],
    'apm' => [
      'APM',
      'logging',
      'metrics',
      'tracing'
    ],
    'health' => [
      'cluster monitoring',
      'error tracking',
      'incident management',
      'infrastructure monitoring',
      'status page',
      'synthetic monitoring'
    ],
    'static analysis' => [
      'sast',
      'secret detection'
    ],
    'dynamic analysis' => [
      'dast',
      'fuzzing',
      'iast'
    ],
    'software composition analysis' => [
      'container scanning',
      'dependency list',
      'dependency scanning',
      'license management',
      'vulnerability management'
    ],
    'runtime application security' => [
      'runtime application security'
    ],
    'threat management' => [
      'behavior analytics',
      'threat detection',
      'vulnerability management'
    ],
    'application infrastructure security' => [
      'container network security',
      'data loss prevention',
      'storage security'
    ],
    'activation' => [],
    'adoption' => [],
    'upsell' => [],
    'retention' => [],
    'fulfillment' => [
      'licensing',
      'telemetry',
      'transactions'
    ],
    'distribution' => [
      'Cloud Native',
      'Omnibus'
    ],
    'geo' => [
      'Geo Administration',
      'Geo DR',
      'Geo GA',
      'Geo Next Gen',
      'Geo Performance',
      'Geo Replication/Sync',
      'Geo Verification/Accuracy',
      'Geo : Automatic Verification',
      'Geo : Kubernetes',
      'Geo : Replication Lag Warning',
      'Geo : Selective Sync'
    ],
    'memory' => [],
    'ecosystem' => [
      'external services',
      'services'
    ]
  }
  CATEGORIES_PER_DEPARTMENT = {
    'Quality' => [
      'ci-build',
      'insights',
      'master:broken',
      'master:flaky',
      'master:needs-investigation',
      'QA',
      'static analysis'
    ]
  }
  CATEGORIES_PER_GROUP_OR_DEPARTMENT = CATEGORIES_PER_GROUP.merge(CATEGORIES_PER_DEPARTMENT)
  TEAM_TO_STAGE = {
    'Manage' => 'manage',
    'Plan' => 'plan',
    'Create' => 'create',
    'Verify' => 'verify',
    'Package' => 'package',
    'Release' => 'release',
    'Configure' => 'configure',
    'Serverless' => 'configure',
    'Monitor' => 'monitor',
    'Secure' => 'secure',
    'Defend' => 'defend',
    'Growth' => 'growth'
  }
  TEAM_TO_GROUP = {
    'Gitaly' => 'gitaly',
    'Gitter' => 'gitter',
    'Distribution' => 'distribution',
    'Geo' => 'geo',
    'Memory' => 'memory',
    'Ecosystem' => 'ecosystem'
  }
  STAGE_LABEL_PREFIX = 'devops::'
  GROUP_LABEL_PREFIX = 'group::'

  def self.stages
    STAGES_PER_SECTION.values.flatten
  end

  def self.groups
    GROUPS_PER_STAGE.values.flatten
  end

  def self.stage_labels
    stages.map do |stage|
      "#{STAGE_LABEL_PREFIX}#{stage}"
    end + departments
  end

  def self.group_labels
    groups.map do |group|
      "#{GROUP_LABEL_PREFIX}#{group}"
    end
  end

  def self.category_labels
    CATEGORIES_PER_GROUP.values.flatten +
      CATEGORIES_PER_DEPARTMENT.values.flatten
  end

  def self.team_labels
    TEAM_TO_STAGE.keys + TEAM_TO_GROUP.keys
  end

  def self.departments
    CATEGORIES_PER_DEPARTMENT.keys
  end

  def self.department?(item)
    departments.include?(item)
  end

  module Context
    extend self

    LabelsInferenceResult = Struct.new(:new_labels, :matching_labels) do
      def any?
        new_labels.any?
      end

      def new_labels
        self[:new_labels] ? Array[self[:new_labels]].flatten.compact : []
      end

      def matching_labels
        self[:matching_labels] ? Array[self[:matching_labels]].flatten : []
      end

      def comment
        "#{explanation}\n/label #{new_labels_markdown}"
      end

      private

      def new_labels_markdown
        new_labels.map(&Context.method(:markdown_label)).join(' ')
      end

      def matching_labels_markdown
        matching_labels.map(&Context.method(:markdown_label)).join(' ')
      end

      def explanation
        "Setting #{new_labels_markdown} based on #{matching_labels_markdown}." if any?
      end
    end

    MatchingGroup = Struct.new(:name, :matching_labels) do
      def in_stage?(stage)
        Context.stage_for_group(name) == stage
      end

      def markdown_labels
        matching_labels.map(&Context.method(:markdown_label)).join(' ')
      end
    end

    def label_names
      @label_names ||= labels.map(&:name)
    end

    def current_stage_label
      (DevopsLabels.stage_labels & label_names).first
    end

    def current_group_label
      (DevopsLabels.group_labels & label_names).first
    end

    def current_category_labels
      DevopsLabels.category_labels & label_names
    end

    def current_team_label
      (DevopsLabels.team_labels & label_names).first
    end

    def current_stage_name
      return unless current_stage_label

      current_stage_label.delete_prefix(DevopsLabels::STAGE_LABEL_PREFIX)
    end

    def current_group_name
      return unless current_group_label

      current_group_label.delete_prefix(DevopsLabels::GROUP_LABEL_PREFIX)
    end

    def all_category_labels_for_stage(stage = current_stage_name)
      return [] unless stage
      return [] unless DevopsLabels::GROUPS_PER_STAGE.key?(stage)

      DevopsLabels::GROUPS_PER_STAGE[stage].flat_map do |group|
        all_category_labels_for_group(group)
      end
    end

    def all_category_labels_for_group(group = current_group_name)
      return [] unless group

      DevopsLabels::CATEGORIES_PER_GROUP.fetch(group, [])
    end

    def has_stage_label?
      !current_stage_label.nil?
    end

    def has_group_label?
      !current_group_label.nil?
    end

    def has_category_label_for_current_stage?
      return false unless has_stage_label?

      !(all_category_labels_for_stage & label_names).empty?
    end

    def has_category_label_for_current_group?
      return false unless has_group_label?

      !(all_category_labels_for_group & label_names).empty?
    end

    def can_infer_labels?
      !current_category_labels.empty? || !current_team_label.nil?
    end

    def default_group_for_stage(stage = nil)
      DevopsLabels::GROUPS_PER_STAGE[stage || current_stage_name]&.first
    end

    def stage_for_group(group = nil)
      detected_stage = DevopsLabels::GROUPS_PER_STAGE.detect do |stage, groups|
        groups.include?(group || current_group_name)
      end

      detected_stage&.first
    end

    def new_stage_and_group_labels
      new_labels =
        if has_stage_label?
          infer_group_from_category unless has_group_label?
        else
          if has_group_label?
            stage_label = label_for(stage: stage_for_group)
            LabelsInferenceResult.new(stage_label, current_group_label)
          else
            infer_stage_and_group_from_category
          end
        end

      new_labels || LabelsInferenceResult.new
    end

    def comment
      new_stage_and_group_labels.comment if new_stage_and_group_labels.any?
    end

    private

    def infer_group_from_category
      matching_group = best_matching_group

      if matching_group && matching_group.in_stage?(current_stage_name)
        LabelsInferenceResult.new(label_for(group: matching_group.name), matching_group.matching_labels)
      elsif stage_matching_groups.none?
        LabelsInferenceResult.new(label_for(group: default_group_for_stage), current_stage_label)
      end
    end

    def infer_stage_and_group_from_category
      matching_group = best_matching_group

      if matching_group
        stage_label = label_for(stage: stage_for_group(matching_group.name))
        group_label = label_for(group: matching_group.name)

        LabelsInferenceResult.new([stage_label, group_label], matching_group.matching_labels)
      else
        infer_stage_and_group_label_from_team_label
      end
    end

    def infer_stage_and_group_label_from_team_label
      group = group_from_team_label
      stage = group ? stage_for_group(group) : stage_from_team_label

      if stage
        stage_label = label_for(stage: stage)
        group_label = label_for(group: group || default_group_for_stage(stage))

        LabelsInferenceResult.new([stage_label, group_label], current_team_label)
      end
    end

    def best_matching_group
      matching_groups = stage_matching_groups.any? ? stage_matching_groups : groups_from_category
      matching_labels_count = matching_groups.map(&:matching_labels).size.to_f

      matching_groups.find do |matching_group|
        (matching_group.matching_labels.size / matching_labels_count) > 0.5
      end
    end

    def groups_from_category
      @groups_from_category ||=
        CATEGORIES_PER_GROUP_OR_DEPARTMENT.each_with_object([]) do |(group, category_labels), matches|
          matching_category_labels = category_labels & label_names
          matches << MatchingGroup.new(group, matching_category_labels) if matching_category_labels.any?
        end
    end

    def stage_matching_groups
      @stage_matching_groups ||=
        groups_from_category.select do |matching_group|
          DevopsLabels.department?(matching_group.name) ||
            stage_for_group(matching_group.name) == current_stage_name
        end
    end

    def stage_from_team_label
      TEAM_TO_STAGE.find do |(team, _), matches|
        label_names.include?(team)
      end&.last
    end

    def group_from_team_label
      TEAM_TO_GROUP.find do |(team, _), matches|
        label_names.include?(team)
      end&.last
    end

    def label_for(stage: nil, group: nil)
      stage_for_group = stage || group
      return stage_for_group if DevopsLabels.department?(stage_for_group)

      if stage
        "#{DevopsLabels::STAGE_LABEL_PREFIX}#{stage}"
      elsif group
        "#{DevopsLabels::GROUP_LABEL_PREFIX}#{group}"
      end
    end

    def markdown_label(label)
      %(~"#{label}")
    end
  end
end
