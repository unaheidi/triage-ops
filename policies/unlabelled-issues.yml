.common_conditions: &common_conditions
  conditions:
    state: opened
    labels:
      - none

.common_rules: &common_rules
  limits:
    most_recent: 45

.common_actions: &common_actions
  summarize:
    item: |
      - [ ] ##{resource[:iid]} {{title}} {{labels}}
    title: |
      #{Date.today.iso8601} Newly created unlabelled issues requiring initial triage
    summary: |
      Hi Triage Team,

      Here is a list of the latest issues without labels in the project.

      In accordance with the [Level 1 triage guidelines](https://about.gitlab.com/handbook/engineering/issue-triage/#level-1-triage-is-partial), we would like to ask you to:

      1. Check for duplicates in this project and others (it is common for issues reported in EE to already exist in CE).
      1. Add a [type label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#type-labels).
        - If identified as a bug, add a [severity label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#severity-labels).
        - If the severity is ~S1 or ~S2, [mention relevant PM/EMs from the relevant stage group from product devstages categories](https://about.gitlab.com/handbook/product/categories/#devops-stages).
      1. Add a [team label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#team-labels).
      1. Add a [stage label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#stage-labels).
      1. Add relevant [subject labels](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#subject-labels) to facilitate automatic addition of stage and group labels.
      1. If needed, [mention relevant domain experts](https://about.gitlab.com/company/team/structure/#expert) if the issue requires further attention.

      For the issues triaged please check off the box in front of the given issue.

      Once you've triaged all the issues assigned to you, you can unassign yourself with the `/unassign me` quick action.

      **When all the checkboxes are done, close the issue, and celebrate!** :tada:

      #{
      potential_triagers = %w[@markglenfletcher @godfat @ddavison @mlapierre @at.ramya @sliaquat @tnikic @zeffmorgan @tpazitny @wlsf82 @dchevalier2 @asoborov @grantyoung @jennielouie].shuffle
      list_items = resource[:items].split("\n")
      items_per_triagers = potential_triagers
        .zip(list_items.each_slice((list_items.size.to_f / potential_triagers.size).ceil))
        .to_h.compact

      items_per_triagers.each_with_object([]) do |(triager, items), text|
        text << "#{triager}\n\n#{items.join("\n")}"
      end.join("\n\n")
      }

      /assign #{items_per_triagers.keys.join(' ')}
      /label ~Quality ~"triage\-package"

resource_rules:
  issues:
    rules:
      - name: Collate latest unlabelled issues
        <<: *common_rules
        <<: *common_conditions
        actions:
          <<: *common_actions
