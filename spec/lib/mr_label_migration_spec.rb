require 'spec_helper'
require 'mr_label_migration'

RSpec.describe MrLabelMigrationContext do
  let(:resource_klass) do
    Struct.new(:labels) do
      include MrLabelMigrationContext
    end
  end
  let(:label_klass) do
    Struct.new(:name)
  end

  describe '#new_stage_and_group_labels and #comment' do
    where(:case_name, :current_labels, :expected_new_stage_and_group_labels, :explanation) do
      [
        ["stage: yes, group: yes, category: yes, single_team: yes => Different stages to legacy label. Do nothing", ["devops::configure", "group::autodevops and kubernetes", "wiki", "Verify"], [], ''],
        ["stage: yes, group: yes, category: yes, single_team: yes => Stage and group already applied. Do nothing", ["devops::verify", "group::ci and runner", "Verify"], [], ''],
        ["stage: yes, group: no, category: yes, single_team: yes => Missing group label for existing stage", ["Gitaly", "devops::create"], ["group::gitaly"], %(Setting ~"group::gitaly" based on ~"Gitaly".)],
        ["stage: no, group: no, category: yes, single_team: yes => Apply stage label from legacy team label", ["wiki", "Verify"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify".)],
        ["stage: no, group: yes, category: yes, single_team: yes => Do not override existing group", ["group::autodevops and kubernetes", "wiki", "Verify"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify".)],
        ["stage: yes, group: no, category: no, single_team: yes => Do not override existing stage", ["devops::configure", "Verify"], [], ''],
        ["stage: no, group: no, category: no, single_team: yes => Apply stage and group label from legacy team label", ["Distribution"], ["devops::enablement", "group::distribution",], %(Setting ~"devops::enablement" ~"group::distribution" based on ~"Distribution".)],
        ["stage: no, group: no, category: no, single_team: yes => Apply stage and group label from legacy team label 2", ["Gitaly"], ["devops::create", "group::gitaly"], %(Setting ~"devops::create" ~"group::gitaly" based on ~"Gitaly".)],
        ["stage: no, group: yes, category: yes, single_team: no => Do nothing if a stage label is missing and multiple legacy labels present", ["group::autodevops and kubernetes", "wiki", "Verify", "Configure"], [], ''],
      ]
    end

    with_them do
      it "returns an array of relevant labels" do
        resource = resource_klass.new(current_labels.map { |l| label_klass.new(l) })

        expect(resource.stage_or_group_label_from_legacy_label.new_labels).to eq expected_new_stage_and_group_labels
      end
    end

    it "returns nil if the resource does not warrant any new labels" do
      resource = resource_klass.new([])

      expect(resource.comment_for_merge_request).to be_nil
    end

    with_them do
      it "returns a comment with a /label quick action" do
        resource = resource_klass.new(current_labels.map { |l| label_klass.new(l) })

        if expected_new_stage_and_group_labels.empty?
          expect(resource.comment_for_merge_request).to be_nil
        else
          expect(resource.comment_for_merge_request).to eq %(#{explanation}\n/label #{expected_new_stage_and_group_labels.map { |l| %(~"#{l}") }.join(' ')})
        end
      end
    end
  end
end
