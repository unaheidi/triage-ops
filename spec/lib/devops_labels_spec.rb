require 'spec_helper'
require 'devops_labels'

RSpec.describe DevopsLabels do
  describe '.stages' do
    it 'returns the stages' do
      stages = described_class.stages

      expect(stages.first).to eq('manage')
      expect(stages.last).to eq('enablement')
    end
  end

  describe '.groups' do
    it 'returns the groups' do
      groups = described_class.groups

      expect(groups.first).to eq('access')
      expect(groups.last).to eq('ecosystem')
    end
  end
  describe '.stage_labels' do
    it 'returns the stage labels' do
      labels = described_class.stage_labels

      expect(labels.first).to eq('devops::manage')
      expect(labels.last).to eq('Quality')
    end
  end

  describe '.group_labels' do
    it 'returns the group labels' do
      labels = described_class.group_labels

      expect(labels.first).to eq('group::access')
      expect(labels[2]).to eq('group::team planning')
      expect(labels.last).to eq('group::ecosystem')
    end
  end

  describe '.category_labels' do
    it 'returns the category labels' do
      labels = described_class.category_labels

      expect(labels.first).to eq('2FA')
      expect(labels.last).to eq('static analysis')
    end
  end

  describe '.team_labels' do
    it 'returns the team labels' do
      labels = described_class.team_labels

      expect(labels.first).to eq('Manage')
      expect(labels.last).to eq('Ecosystem')
    end
  end

  describe '.departments' do
    it 'returns the departments' do
      departments = described_class.departments

      expect(departments.first).to eq('Quality')
    end
  end

  describe '.department?' do
    it 'returns true when given a department' do
      expect(described_class.department?('Quality')).to eq(true)
    end

    it 'returns true when given a department' do
      expect(described_class.department?('Foo')).to eq(false)
    end
  end

  describe described_class::Context do
    let(:resource_klass) do
      Struct.new(:labels) do
        include DevopsLabels::Context
      end
    end
    let(:label_klass) do
      Struct.new(:name)
    end

    describe '#label_names' do
      it 'returns [] if the resource has no label' do
        resource = resource_klass.new([])

        expect(resource.label_names).to eq([])
      end

      it 'returns the label names if the resource has labels' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_labels.first)])

        expect(resource.label_names).to eq([DevopsLabels.stage_labels.first])
      end
    end

    describe '#current_stage_label' do
      it 'returns nil if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.current_stage_label).to be_nil
      end

      it 'returns the stage label if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_labels.first)])

        expect(resource.current_stage_label).to eq(DevopsLabels.stage_labels.first)
      end
    end

    describe '#current_group_label' do
      it 'returns nil if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.current_group_label).to be_nil
      end

      it 'returns the group label if the resource has a group label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.group_labels.first)])

        expect(resource.current_group_label).to eq(DevopsLabels.group_labels.first)
      end
    end

    describe '#current_team_label' do
      it 'returns nil if the resource has no team label' do
        resource = resource_klass.new([])

        expect(resource.current_team_label).to be_nil
      end

      it 'returns the team name if the resource has a team label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.team_labels.first)])

        expect(resource.current_team_label).to eq(DevopsLabels.team_labels.first)
      end
    end

    describe '#current_category_labels' do
      it 'returns [] if the resource has no category label' do
        resource = resource_klass.new([])

        expect(resource.current_category_labels).to eq([])
      end

      it 'returns the category labels if the resource has a category label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.category_labels.first)])

        expect(resource.current_category_labels).to eq([DevopsLabels.category_labels.first])
      end
    end

    describe '#current_stage_name' do
      it 'returns nil if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.current_stage_name).to be_nil
      end

      it 'returns the stage name if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_labels.first)])

        expect(resource.current_stage_name).to eq(DevopsLabels.stages.first)
      end
    end

    describe '#current_group_name' do
      it 'returns nil if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.current_group_name).to be_nil
      end

      it 'returns the group name if the resource has a group label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.group_labels.first)])

        expect(resource.current_group_name).to eq(DevopsLabels.groups.first)
      end
    end

    describe '#all_category_labels_for_stage' do
      it 'returns [] if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.all_category_labels_for_stage).to eq([])
      end

      it 'returns the stage category labels if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new('devops::release')])

        expect(resource.all_category_labels_for_stage).to include('continuous delivery', 'feature flags')
        expect(resource.all_category_labels_for_stage).not_to include('logging')
      end

      it 'returns the stage category labels when given a stage name' do
        resource = resource_klass.new([])

        expect(resource.all_category_labels_for_stage('release')).to include('continuous delivery', 'feature flags')
        expect(resource.all_category_labels_for_stage('release')).not_to include('logging')
      end
    end

    describe '#all_category_labels_for_group' do
      it 'returns [] if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.all_category_labels_for_group).to eq([])
      end

      it 'returns the group category labels if the resource has a group label' do
        resource = resource_klass.new([label_klass.new('group::progressive delivery')])

        expect(resource.all_category_labels_for_group).to include('continuous delivery')
        expect(resource.all_category_labels_for_group).not_to include('feature flags')
      end

      it 'returns the stage category labels when given a group name' do
        resource = resource_klass.new([])

        expect(resource.all_category_labels_for_group('progressive delivery')).to include('continuous delivery')
        expect(resource.all_category_labels_for_group('progressive delivery')).not_to include('feature flags')
      end
    end

    describe '#has_stage_label?' do
      it 'returns false if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_stage_label
      end

      it 'returns true if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_labels.first)])

        expect(resource).to be_has_stage_label
      end
    end

    describe '#has_group_label?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_group_label
      end

      it 'returns true if the resource has a group label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.group_labels.first)])

        expect(resource).to be_has_group_label
      end
    end

    describe '#has_category_label_for_current_stage?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_category_label_for_current_stage
      end

      it 'returns true if the resource has a stage label and a corresponding category label' do
        resource = resource_klass.new([label_klass.new('devops::release'), label_klass.new('continuous delivery')])

        expect(resource).to be_has_category_label_for_current_stage
      end

      it 'returns true if the resource has a stage label but no corresponding category label' do
        resource = resource_klass.new([label_klass.new('devops::release'), label_klass.new('logging')])

        expect(resource).not_to be_has_category_label_for_current_stage
      end
    end

    describe '#has_category_label_for_current_group?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_category_label_for_current_group
      end

      it 'returns true if the resource has a group label and a corresponding category label' do
        resource = resource_klass.new([label_klass.new('group::progressive delivery'), label_klass.new('continuous delivery')])

        expect(resource).to be_has_category_label_for_current_group
      end

      it 'returns false if the resource has a group label but no corresponding category label' do
        resource = resource_klass.new([label_klass.new('group::progressive delivery'), label_klass.new('feature flags')])

        expect(resource).not_to be_has_category_label_for_current_group
      end
    end

    describe '#can_infer_labels?' do
      it 'returns false if the resource has no category nor team label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_can_infer_labels
      end

      it 'returns true if the resource has a category label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.category_labels.first)])

        expect(resource).to be_can_infer_labels
      end

      it 'returns true if the resource has a team label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.team_labels.first)])

        expect(resource).to be_can_infer_labels
      end
    end

    describe '#default_group_for_stage' do
      it 'returns nil if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.default_group_for_stage).to be_nil
      end

      it 'returns the default group if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new('devops::enablement')])

        expect(resource.default_group_for_stage).to eq('distribution')
      end

      it 'returns the default group of the given stage' do
        resource = resource_klass.new([])

        expect(resource.default_group_for_stage('enablement')).to eq('distribution')
      end
    end

    describe '#stage_for_group' do
      it 'returns nil if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.stage_for_group).to be_nil
      end

      it 'returns the stage name if the resource has a group label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.group_labels.first)])

        expect(resource.stage_for_group).to eq(DevopsLabels.stages.first)
      end

      it 'returns the stage name of the given group' do
        resource = resource_klass.new([])

        expect(resource.stage_for_group('distribution')).to eq('enablement')
      end
    end

    describe '#new_stage_and_group_labels and #comment' do
      where(:case_name, :current_labels, :expected_new_stage_and_group_labels, :explanation) do
        [
          ["stage: yes, group: yes, category: yes, team: yes => No new labels.", ["devops::configure", "group::autodevops and kubernetes", "wiki", "Verify"], [], ''],
          ["stage: yes, group: yes, category: yes, team: no => No new labels.", ["devops::configure", "group::autodevops and kubernetes", "wiki"], [], ''],

          ["stage: yes, group: yes, category: no, team: yes => No new labels.", ["devops::configure", "group::autodevops and kubernetes", "Verify"], [], ''],
          ["stage: yes, group: yes, category: no, team: no => No new labels.", ["devops::configure", "group::autodevops and kubernetes"], [], ''],

          ["stage: yes, group: no, category: yes, team: yes (100% matching stage) => Group based on category since category matches stage", ["devops::create", "wiki", "Verify"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: no (100% matching stage) => Group based on category since category matches stage", ["devops::create", "wiki"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: yes (100% matching stage, 2 different groups) => Manual triage required", ["devops::create", "wiki", "elasticsearch", "Verify"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (100% matching stage, 2 different groups) => Manual triage required", ["devops::create", "wiki", "elasticsearch"], [], ''],
          ["stage: yes, group: no, category: yes, team: yes (66% matching stage) => Group based on category since category matches stage", ["devops::create", "wiki", "design management", "analytics", "Verify"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"design management" ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: no (66% matching stage) => Group based on category since category matches stage", ["devops::create", "wiki", "design management", "analytics"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"design management" ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: yes (50% matching stage) => Group based on category since category matches stage", ["devops::create", "wiki", "analytics", "Verify"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage) => Group based on category since category matches stage", ["devops::create", "wiki", "analytics"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: yes (33% matching stage) => Group based on category since category matches stage", ["devops::create", "wiki", "analytics", "epics", "Verify"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Group based on category since category matches stage", ["devops::create", "wiki", "analytics", "epics"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: yes (none matching stage) => Group based on stage since category does not match stage", ["devops::package", "design management", "Verify"], ["group::package"], %(Setting ~"group::package" based on ~"devops::package".)],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Group based on stage since category does not match stage", ["devops::package", "design management"], ["group::package"], %(Setting ~"group::package" based on ~"devops::package".)],

          ["stage: yes, group: no, category: no, team: yes => Group based on stage", ["Gitaly", "devops::configure"], ["group::autodevops and kubernetes"], %(Setting ~"group::autodevops and kubernetes" based on ~"devops::configure".)],
          ["stage: yes, group: no, category: no, team: no => Default group from stage", ["devops::verify"], ["group::ci and runner"], %(Setting ~"group::ci and runner" based on ~"devops::verify".)],

          ["stage: no, group: yes, category: yes, team: yes => Stage based on group", ["design management", "group::source code", "markdown", "Verify"], ["devops::create"], %(Setting ~"devops::create" based on ~"group::source code".)],
          ["stage: no, group: yes, category: yes, team: no => Stage based on group", ["design management", "group::source code", "markdown"], ["devops::create"], %(Setting ~"devops::create" based on ~"group::source code".)],

          ["stage: no, group: yes, category: no, team: yes => Stage label based on group", ["Gitaly", "group::memory"], ["devops::enablement"], %(Setting ~"devops::enablement" based on ~"group::memory".)],
          ["stage: no, group: yes, category: no, team: no => Stage label based on group", ["group::memory"], ["devops::enablement"], %(Setting ~"devops::enablement" based on ~"group::memory".)],

          ["stage: no, group: no, category: no, team: yes => Stage and group based on team", ["Plan", "backend", "bug"], ["devops::plan", "group::team planning"], %(Setting ~"devops::plan" ~"group::team planning" based on ~"Plan".)],
          ["stage: no, group: no, category: no, team: no => Manual triage required", ["bug", "rake tasks"], [], ''],

          ["stage: no, group: no, category: yes, team: yes (best match: 100%) => Stage and group based on category", ["internationalization", "Verify"], ["devops::manage", "group::measure"], %(Setting ~"devops::manage" ~"group::measure" based on ~"internationalization".)],
          ["stage: no, group: no, category: yes, team: no (best match: 100%) => Stage and group based on category", ["internationalization"], ["devops::manage", "group::measure"], %(Setting ~"devops::manage" ~"group::measure" based on ~"internationalization".)],
          ["stage: no, group: no, category: yes, team: yes (best match: 66%) => Stage and group based on category label", ["snippets", "elasticsearch", "internationalization", "Verify"], ["devops::create", "group::editor"], %(Setting ~"devops::create" ~"group::editor" based on ~"elasticsearch" ~"snippets".)],
          ["stage: no, group: no, category: yes, team: no (best match: 66%) => Stage and group based on category label", ["snippets", "elasticsearch", "internationalization"], ["devops::create", "group::editor"], %(Setting ~"devops::create" ~"group::editor" based on ~"elasticsearch" ~"snippets".)],
          ["stage: no, group: no, category: yes, team: yes (best match: 50%) => Stage and group based on team", ["elasticsearch", "internationalization", "Verify"], ["devops::verify", "group::ci and runner"], %(Setting ~"devops::verify" ~"group::ci and runner" based on ~"Verify".)],
          ["stage: no, group: no, category: yes, team: no (best match: 50%) => Manual triage required", ["elasticsearch", "internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: yes (best match: 33%) => Stage and group based on team", ["pipeline", "elasticsearch", "internationalization", "Verify"], ["devops::verify", "group::ci and runner"], %(Setting ~"devops::verify" ~"group::ci and runner" based on ~"Verify".)],
          ["stage: no, group: no, category: yes, team: no (best match: 33%) => Manual triage required", ["pipeline", "elasticsearch", "internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: yes (no match) => Stage and group based on team", ["backstage", "Verify"], ["devops::verify", "group::ci and runner"], %(Setting ~"devops::verify" ~"group::ci and runner" based on ~"Verify".)],
          ["stage: no, group: no, category: yes, team: no (no match) => Manual triage required", ["backstage"], [], ''],

          ["~insights => ~Quality", ["insights"], ["Quality"], %(Setting ~"Quality" based on ~"insights".)],
          ["~Quality ~insights => No new labels", ["Quality", "insights"], [], ''],
        ]
      end

      with_them do
        it "returns an array of relevant labels" do
          resource = resource_klass.new(current_labels.map { |l| label_klass.new(l) })

          expect(resource.new_stage_and_group_labels.new_labels).to eq expected_new_stage_and_group_labels
        end
      end

      it "returns nil if the resource does not warrant any new labels" do
        resource = resource_klass.new([])

        expect(resource.comment).to be_nil
      end

      with_them do
        it "returns a comment with a /label quick action" do
          resource = resource_klass.new(current_labels.map { |l| label_klass.new(l) })

          if expected_new_stage_and_group_labels.empty?
            expect(resource.comment).to be_nil
          else
            expect(resource.comment).to eq %(#{explanation}\n/label #{expected_new_stage_and_group_labels.map { |l| %(~"#{l}") }.join(' ')})
          end
        end
      end
    end
  end
end
