# frozen_string_literal: true

require_relative File.expand_path('../lib/mr_label_migration.rb', __dir__)

Gitlab::Triage::Resource::Context.include MrLabelMigrationContext
